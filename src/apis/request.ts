import axios from "axios"
import Configs from "react-native-config"

import { requestLog } from "./logger"
import TokenService, { TOKEN_SERVICES_TYPES } from "./tokenServices"

const headers = {
  "Content-Type": "application/json",
  CompanyId: Configs.COMPANY_ID,
  Namespace: Configs.NAME_SPACE,
  Secretkey: Configs.SECRET_KEY,
}

const Request = axios.create({
  baseURL: Configs.API_URL,
  timeout: 10000,
  headers,
})

Request.interceptors.request.use(
  (config) => {
    const token = TokenService.getLocalAccessToken()
    if (token) {
      config.headers!.Authorization = `Bearer ${token}` // for Spring Boot back-end
    }

    return config
  },
  (error) => {
    return Promise.reject(error)
  },
)

Request.interceptors.response.use(
  (res) => {
    return res
  },
  async (err) => {
    const originalConfig = err.config

    if (originalConfig.url !== "/auth/signin" && err.response) {
      // Access Token was expired
      if (err.response.status === 401 && !originalConfig._retry) {
        originalConfig._retry = true

        try {
          const rs = await Request.post("/auth/refreshtoken", {
            refreshToken: TokenService.getLocalRefreshToken(),
          })

          const { accessToken, refreshToken } = rs.data as TOKEN_SERVICES_TYPES
          TokenService.updateLocalTokenServices({ accessToken, refreshToken })

          return Request(originalConfig)
        } catch (_error) {
          return Promise.reject(_error)
        }
      }
    }

    return Promise.reject(err)
  },
)

Request.interceptors.request.use(
  (req) => {
    requestLog(req.method, req.data.fn, req, "req")

    return req
  },
  (error) => {
    return error
  },
)

Request.interceptors.response.use(
  (res) => {
    requestLog(res.config.method, res.config.data, res, "res")

    return res
  },
  (error) => {
    return Promise.reject(error)
  },
)

// export const setAccessToken = (token?: string) => {
//   if (token) {
//     Request.defaults.headers.Authorization = token
//   }
// }

// export const clearAccessToken = () => {
//   Request.defaults.headers.Authorization = undefined
// }

// export const setLanguageRequest = (SourceLanguage: string, targetLanguage: string) => {
//   Request.defaults.headers.SourceLanguage = SourceLanguage
//   Request.defaults.headers.targetLanguage = targetLanguage
// }

export default Request
